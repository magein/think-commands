### 更新日志

#### v2.0.4

> 2024-09-30

1. 优化数据mock代码
2. 优化comment代码

#### v2.0.3

> 2024-08-16

1. 新增MakeFaker中ip、username、email、mobile、tel字段的识别

#### v2.0.2

> 2024-07-23

1. 修改use CommandParamParse 引入的命名空间错误

#### v2.0.1

> 2024-07-17

1. 修改MakeModel.php转化状态类的引入命名空间错误
2. 修改makeRepository.php引入命名空间错误的问题以及修改help中描述文案不严谨的问题

#### v2.0.0

1. 修改命名空间以及目录规范
2. 支持composer extra扩展，安装即用，不需要在console.php中添加命令

#### v1.0.3

> 2024-01-25

1. 重构了代码，支持new MakeXXX()的方式调用，以便于快速获取生成的模版文件
2. 优化 MakeValidate 中varchar长度取值的问题，由固定1~100修改成从varchar(xx)中获取设置的长度
3. 优化 MakeValidate 中tinyint生成in的取值范围，从comment中提取所有的整数型作为可选值
4. 优化 Faker 中对于生成图片跟url地址的支持，需要在配置文件config/console.php增加配置,参考上述配置文件中的config['faker']
   配置项
5. 修复 MakeComment 中生成分页和列表参数无效的问题

#### v1.0.2

1. 1.0.1中的MakeModel.php 取消了status字段的对 trait 的依赖，在v1.0.2中恢复，会出现ModelStatusField找不到的问题

这文件在magein/think-utils中，之所以在另外一个包中，还是要使用trait的特性

#### v1.0.1

1. 优化MakeModel.php 取消了status字段的对trait的依赖，将status字段的set、get、translate直接生成在模型中
2. 修复MakeFaker.php中调用Str::random()引起的错误
3. 修复MakeFaker.php中created_at默认值引起的错误
4. 修改MakeComment.php中保存的默认路径