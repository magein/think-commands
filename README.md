# Thinkphp6 , Commands

### 简介

thinkphp6~ 开发中使用的命令行用于快速创建文件

[lastest version v2.0.4](https://gitee.com/magein/think-commands/tags) |  [composer](https://packagist.org/packages/magein/think-command)  | [gitee](https://gitee.com/magein/think-commands)

### 安装

因为是快速创建文件，所以仅仅是开发使用，切勿在生产环境使用

```php
composer require --dev magein/think-command
```

### 配置文件

console.php中可以添加以下配置

```php

'config' => [

    // 创建model模型使用的配置
    'model' => [
        // 保存路径 默认是 app/model
        'path' => 'app/model',
        // 继承的基类
        'extends' => \magein\think\utils\SoftModel::class,
        // 是否自动分组，是文件夹分组，建议开启，以便于管理
        'group' => true,
    ],

    // 数据仓库文件地址
    'repos' => [
        // 保存路径 默认是 app/repository
        'path' => 'app/api/repository',
        // 获取用户的id
        'user' => \app\api\service\UserCenterService::class,
    ],

    // 验证类文件地址
    'validate' => [
        // 保存路径 默认是 app/validate
        'path' => 'app/api/validate',
    ],
    
    // apipost使用的接口字段描述
    'comment' => [
        // 保存路径 默认是comments
        'path' => 'comments',
    ]
    
    // 生成测试数据的配置
    'faker' => [
        //  识别成图片字段 默认['avatar', 'head', 'icon', 'pic', 'pics', 'img', 'image', 'images', 'picture', 'photo']
        'img_field' => ['img','icon'],
        //  图片地址可选值
        'img_src' => [
            '/static/img/user.png',
            '/static/img/default.png',
        ],
        // 识别成http远程地址 默认['url', 'urls', 'redirect', 'http', 'https', 'remote', 'remote_url', 'domain', 'host']
        'http_field' => [],
        // 远程地址可选值
        'http_url' => [
            env('app_url', 'http://localhost'),
            'https://xxxx.com',
        ],
     ],
]
```

### 使用

#### 快速创建模型

> 需要指定数据中的表名称

```
# 创建model类且继承MainModel，同时执行model:attr
php think model:new  users
# 创建model属性
php think model:attr user

```

可以使用--help查看帮助说明

```
php think model:new --help
php think model:attr --help
```

#### 创建模型的属性

> 需要指定已经创建的模型名称

创建属性是为了更好的贴切对面对象的编程模式,且ide中获取提示

```php
$user=User::find();
echo $user->name;
```

```php
php think model:attr user
```

user 查找 app/model/User.php

```php
php think model:attr user/user_address
```

user/user_address 查找 就是在app/model/user/UserAddress.php

#### 创建验证类

跟thinkphp官方的命令不一样的地方是会根据模型对应的表字段进行创建

> 需要指定已经创建的模型名称

```php
php think model:vi user
```

#### 创建测试数据

> 使用模型创建测试数据

```php
php think model:faker user -l 5
```

#### 创建字段说明文件

> 仅仅针对于apipost这个工具生成的描述字段

> 使用模型创建测试数据

```php
php think model:comment user
php think model:comment user -l
php think model:comment user -p
```

#### 创建api仓库

使用命令需要安装magein/think-utils

```php
composer magein/think-utils
```

```php
php think model:repos user
```


