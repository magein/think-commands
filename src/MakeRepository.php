<?php

/**
 * @user magein
 * @date 2023/12/7 9:40
 */

namespace magein\think\command;

use magein\think\command\traits\CommandParamParse;

use magein\utils\Variable;
use think\console\Command;
use think\console\Input;
use think\console\Output;
use think\Model;

class MakeRepository extends Command
{

    use CommandParamParse;

    protected $help = '
    api使用的数据仓库文件,创建到到配置的目录下
    在config/console.php文件中配置config.repos.path参数值
    
    php think model:repos user                      创建repository/UserRepos.php文件
    php think model:repos user_order                创建repository/UserOrderRepos.php创建
    php think model:repos user/user_order           创建repository/user/UserOrderRepos.php创建
    
    附：仓库目录默认是app/repository，可在config/console.php文件中配置config.repository.path参数的值
';

    protected function configure()
    {
        // 指令配置
        $this->setName('model:repos')
            ->addArgument('name')
            ->setDescription('the model repository command')
            ->setHelp($this->help);
    }

    protected function execute(Input $input, Output $output)
    {
        if (env('app_env') !== 'local') {
            $output->error('只能在开发环境中使用');
            exit();
        }

        $name = $input->getArgument('name');

        // 根据参数获取模型实例
        $model = $this->model($name);
        if (empty($model) || !$model instanceof Model) {
            $output->error("没有找到{$name}对应的模型文件");
            exit();
        }

        // 保存路径
        $filepath = $this->filepath($name, 'repos', 'app/repository');
        // 文件的命名空间
        $namespace = $this->namespace($filepath);
        $filename = $this->filename($name, 'Repos');
        $filepath .= Variable::ins()->pascal(trim($filename, '/')) . '.php';
        if (is_file($filepath)) {
            $output->error($filepath . '已经存在');
            exit();
        }

        $data = $this->make($model, $namespace);
        $template = $this->template($model, $data);

        file_put_contents($filepath, $template);
        // 指令输出
        $output->writeln($filepath . '创建成功');

        if ($data['validate']) {
            \think\facade\Console::call('model:vi', [$name]);
        }

        \think\facade\Console::call('model:comment', [$name]);
    }

    /**
     * 生成仓库所需要的属性
     * @param Model $model
     * @param string|null $namespace 使用的命名空间
     * @return array
     */
    public function make(Model $model, string $namespace = null): array
    {
        $namespace = $namespace ?: 'app\api\repository';
        $attrs = $this->tableAttrs($model);

        $model_name = pathinfo(get_class($model), PATHINFO_BASENAME);
        $path = app()->config->get("console.config.model.path", 'app\model');

        $name = str_replace($path, '', get_class($model));
        $name = trim($name, '/');
        $name = trim($name, '\\');

        $validate_path = $this->filepath($name, 'validate', 'app/validate');
        $validate_name = $model_name . 'Validate';
        $validate_namespace = $this->namespace($validate_path) . '\\' . $validate_name;

        $user = '';
        $accepts = '';
        $condition = '';
        foreach ($attrs as $attr) {
            $field = $attr['Field'];

            if (in_array($field, ['user_id', 'member_id'])) {
                $user = '$setting->setUser();';
            }

            if (in_array($field, ['created_at', 'updated_at', 'deleted_at'])) {
                continue;
            }
            if (empty($accepts)) {
                $accepts .= "'$field',\n";
            } else {
                $accepts .= "            '$field',\n";
            }

            if ($field === 'sort') {
                $condition .= '        $' . "setting->setSort('sort asc');\n";
            } elseif ($field === 'begin_time' || $field === 'start_time') {
                $condition .= '        $' . "setting->setCondition('$field', '<', date('Y-m-d H:i:s'));\n";
            } elseif ($field === 'end_time') {
                $condition .= '        $' . "setting->setCondition('$field', '>', date('Y-m-d H:i:s'));\n";
            } elseif ($field === 'status') {
                $condition .= '        $' . "setting->setCondition('status', 1);\n";
            }
        }
        $fields = trim($accepts, "\n");
        $fields .= "\n            'created_at'";
        $fields = trim($fields);

        $condition = trim($condition);

        $validate = '';
        $allow_save = '';
        if (empty($user)) {
            $accepts = '';
            $user = '$setting->setUser(null);';
            $validate_namespace = '';
        } else {
            $validate = '$setting->setValidate(' . $validate_name . '::class);';
            $validate_namespace = 'use ' . $validate_namespace . ';';
            $allow_save = '$setting->allowSave();';
        }

        if ($accepts) {
            $accepts = <<<EOF
\$accept = [
            $accepts
        ];
EOF;
        } else {
            $accepts = <<<EOF
\$accept = [];
EOF;
        }

        $accepts = trim($accepts, "\n");

        return compact(
            'namespace',
            'user',
            'fields',
            'condition',
            'validate',
            'validate_namespace',
            'accepts',
            'allow_save'
        );
    }

    /**
     * 模版
     * @param Model $model
     * @param $params
     * @return string
     */
    public function template(Model $model, $params = null): string
    {

        $params = $params ?: $this->make($model);

        extract($params);

        $model_name = pathinfo(get_class($model), PATHINFO_BASENAME);
        $model_namespace = get_class($model);
        $cla_name = $model_name . 'Repos';

        return <<<EOF
<?php

declare (strict_types=1);

namespace $namespace;

use magein\\think\utils\\repository\InputSetting;
use magein\\think\utils\\repository\OutputSetting;
use magein\\think\utils\\repository\DeleteSetting;

use magein\\think\utils\\repository\Repository;

use $model_namespace;
$validate_namespace

class {$cla_name} extends Repository
{
    /**
     * @return string
     */
    public function model(): string
    {
        return $model_name::class;
    }

    /**
     * @return OutputSetting
     */
    public function output(): OutputSetting
    {
        \$fields = [
            $fields
        ];
    
        \$setting = new OutputSetting();
        \$setting->setFields(\$fields);
        $condition
        \$setting->setPaginate();
        $user

        return \$setting;
    }
    
    /**
     * @return InputSetting
     */
    public function input(): InputSetting
    {
        $accepts
        \$setting = new InputSetting();
        \$setting->setAccept(\$accept);
        $user
        $validate
        $allow_save

        return \$setting;
    }

    /**
     * @return DeleteSetting
     */
    public function delete(): DeleteSetting
    {
        \$setting = new DeleteSetting();
        \$setting->allow(false);
        return \$setting;
    }
}
EOF;
    }
}