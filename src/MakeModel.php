<?php
declare (strict_types=1);

namespace magein\think\command;

use magein\utils\Variable;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument as InputArgument;
use think\console\input\Option;
use think\console\Output;
use think\db\exception\DbException;
use think\facade\Db;

class MakeModel extends Command
{

    protected $help = '
    php think model:new users                      创建model/User.php
    php think model:new user_orders                创建model/UserOrder.php
    php think model:new user_orders -p             创建model/User/UserOrder.php
    php think model:new user_orders --path         创建model/User/UserOrder.php
    php think model:new user_orders -e think       创建model/UserOrder.php并且继承think的model
';

    protected function configure()
    {
        // 指令配置
        $this->setName('model:new')
            ->setDescription('快速生成模型')
            ->addArgument('table_name', InputArgument::REQUIRED, '数据库表名称')
            ->addOption('group', 'g', Option::VALUE_NONE, '生成递归文件目录')
            ->setHelp($this->help);
    }

    protected function execute(Input $input, Output $output)
    {

        if (env('app_env') !== 'local') {
            $output->error('只能在开发环境中使用');
            exit();
        }

        $table_name = $input->getArgument('table_name');
        $group = $input->getOption('group');
        $config = $this->app->config->get('console.config.model', []);

        // 基本路径
        $base_path = $config['path'] ?? '' ?: 'app/model';

        if ($group === false) {
            $group = $config['group'] ?? false;
        }

        if (empty($table_name)) {
            $output->info($this->getHelp());
            exit(1);
        }

        try {
            $attrs = Db::query("show full columns from $table_name");
        } catch (DbException $exception) {
            $output->error("没有检测到{$table_name}表字段信息，请检查表名称,输入完整的表名称");
            exit(1);
        }

        $name = $table_name;

        if ($group) {
            $params = explode('_', $name);
            if (count($params) > 1) {
                $base_path .= '/' . $params[0];
            }
        }

        if (!is_dir($base_path) && !mkdir($base_path, 0777, true)) {
            $output->error("创建模型目录失败{$base_path}，请先创建该目录");
            exit(1);
        }

        $base_path = trim(trim($base_path, '.'), '/');
        $namespace = str_replace('/', '\\', $base_path);

        $class_name = $name;
        if (preg_match('/ies$/', $class_name)) {
            $class_name = preg_replace('/ies$/', 'y', $class_name);
        } elseif (preg_match('/s$/', $class_name)) {
            $class_name = substr($class_name, 0, -1);
        }

        $class_name = Variable::ins()->pascal($class_name);
        $filename = $base_path . '/' . $class_name . '.php';

        $uses = [];

        $const = '';

        $traits = [];

        $trans = [];

        if ($attrs) {
            foreach ($attrs as $attr) {
                $field = $attr['Field'];
                $type = $attr['Type'];
                $comment = $attr['Comment'];
                if (preg_match('/tinyint/', $type)) {

                    $comments = explode(' ', $comment);

                    /**
                     * 过滤空格(注意0值)
                     */
                    $comments = array_filter($comments, function ($value) {
                        if ($value === '') {
                            return false;
                        }
                        return true;
                    });

                    $is_status = false;
                    if (preg_match('/forbid/', $comment)) {
                        $is_status = true;
                    }

                    if ($field === 'status' && count($comments) === 7 && $is_status) {
                        $uses[] = 'magein\think\utils\traits\StatusTranslate';
                        $traits[] = 'use StatusTranslate;';
                    } else {

                        $desc = $comments[0];

                        array_shift($comments);

                        /**
                         * 是否是三的倍数
                         */
                        $normal = count($comments) % 3;
                        if ($normal === 0) {

                            $comments = array_chunk($comments, 3);
                            foreach ($comments as $comment) {
                                $second = $comment[1];
                                $v = $comment[0];
                                //拼接常量
                                $format = sprintf('%s_%s', strtoupper($field), strtoupper($comment[2]));
                                $const .= <<<EOF

    /**
     * $desc:$second
     */
    const $format = $v;
 
EOF;

                                $trans[$field][] = [$format, $second];
                            }
                        }
                    }
                }
            }
        }

        $extends = $config['extends'] ?? '';
        if ($extends) {
            $uses[] = $extends;
            $extends = explode('\\', $extends);
            $extends = end($extends);
        } else {
            $uses[] = 'think\Model';
            $extends = 'Model';
        }

        if (is_file($filename)) {
            $output->error('file exists:' . $filename);
            exit();
        }

        $trait_string = '';
        foreach ($traits as $trait) {
            $trait_string .= $trait . "\n";
        }

        $use_string = '';
        foreach ($uses as $use) {
            $use_string .= 'use ' . $use . ";\n";
        }

        $trans_string = '';
        foreach ($trans as $key => $value) {

            $f = Variable::ins()->pascal($key);

            $options = "\n";
            foreach ($value as $val) {
                $options .= <<<EOF
            self::$val[0] => '$val[1]',
EOF;
                $options .= "\n";
            }
            $options = substr($options, 0, strlen($options) - 2);

            // 拼接转化
            $trans_string .= <<<EOF

    

    /**
     * @param string|int|null \$value
     * @return string|array
     */
    public static function translate{$f}(\$value = null)
    {
        \$data = [$options
        ];

        if (\$value !== null) {
            return \$data[\$value] ?? '';
        }

        return \$data;
    }
    
    /**
     * @param \$value
     * @return int
     */
    public function set{$f}Attr(\$value): int
    {
        return intval(\$value);
    }
    
    /**
     * @param \$value
     * @param \$data
     * @return string
     */
    public function get{$f}TextAttr(\$value, \$data)
    {
        \$value = \$data['$key'] ?? '';
        if (empty(\$value)) {
            return '';
        }

        return self::translate{$f}(\$value);
    }

EOF;
        }

        $content = <<<EOF
<?php

declare (strict_types = 1);

namespace $namespace;

$use_string

class {$class_name} extends $extends
{
    
    protected \$table = '$table_name';
    
    $trait_string
    
    $const
    $trans_string
}
EOF;

        file_put_contents($filename, $content);

        $output->writeln('make model successful');

        \think\facade\Console::call('model:attr');
    }
}
