<?php

/**
 * @user magein
 * @date 2023/12/1 15:48
 */

namespace magein\think\command\traits;

use magein\utils\Variable;
use think\db\exception\DbException;
use think\facade\Db;
use think\Model;

trait CommandParamParse
{
    /**
     * @param $name
     * @return  string|null
     */
    public function modelNamespace($name): ?string
    {
        $config = app()->config->get('console.config.model', []);

        $path = ($config['path'] ?? '') ?: 'app/model';

        $names = explode('/', $name);
        $filename = array_pop($names);
        $filepath = trim($path, '/') . '/';
        foreach ($names as $item) {
            $filepath .= Variable::ins()->camelCase($item) . '/';
        }
        $filepath .= Variable::ins()->pascal(trim($filename, '/')) . '.php';

        $namespace = preg_replace(['/\.\/app/', '/\.php/', '/\//'], ['app', '', '\\\\'], $filepath);

        if (class_exists($namespace)) {
            return $namespace;
        }

        return null;
    }

    /**
     * 获取模型数据
     * @param $name
     * @return mixed|null
     */
    public function model($name)
    {
        $namespace = $this->modelNamespace($name);

        if (empty($namespace)) {
            return null;
        }

        return new $namespace();
    }

    /**
     * @param $name
     * @return string
     */
    public function tableName($name): ?string
    {
        $model = $this->model($name);

        if (empty($model)) {
            return null;
        }

        $model = new $model();

        return $model->getTable();
    }

    /**
     * @param $name
     * @return array
     */
    public function tableAttrs($name): array
    {
        if ($name instanceof Model) {
            $table_name = $name->getTable();
        } elseif (is_string($name)) {
            $table_name = $this->tableName($name);
        }

        try {
            $attrs = Db::query("show full columns from $table_name");
        } catch (DbException $queryException) {
            $attrs = [];
        }

        return $attrs;
    }

    /**
     * 获取保存文件路径不存在则创建
     * @param $name
     * @param $type
     * @param $default
     * @return string
     */
    public function filepath($name, $type, $default): string
    {
        $path = app()->config->get("console.config.$type.path", []);

        $path = $path ?: $default;

        $names = explode('/', $name);
        array_pop($names);
        $filepath = trim($path, '/') . '/';
        foreach ($names as $item) {
            $filepath .= Variable::ins()->camelCase($item) . '/';
        }

        if (!is_dir($filepath)) {
            mkdir($filepath, 0777, true);
        }

        return $filepath;
    }

    /**
     * 获取filename
     * @param $name
     * @param string $suffix
     * @return string
     */
    public function filename($name, string $suffix = ''): string
    {
        $names = explode('/', $name);
        $filename = array_pop($names);

        return Variable::ins()->camelCase($filename) . Variable::ins()->pascal($suffix);
    }

    /**
     * 根据文件名称获取命名空间
     * @param $filepath
     * @return array|string|string[]
     */
    public function namespace($filepath)
    {
        // 文件的命名空间
        return str_replace('/', '\\', trim($filepath, '/'));
    }
}