<?php
declare (strict_types=1);

namespace magein\think\command\service;

use magein\think\command\MakeComment;
use magein\think\command\MakeFaker;
use magein\think\command\MakeModel;
use magein\think\command\MakeModelAttr;
use magein\think\command\MakeRepository;
use magein\think\command\MakeValidate;
use think\Service;

class CommandService extends Service
{
    /**
     * 注册服务
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * 执行服务
     *
     * @return void
     */
    public function boot()
    {
        // 添加命令
        $this->commands(
            [
                'model:new' => MakeModel::class,
                'model:attr' => MakeModelAttr::class,
                'model:repos' => MakeRepository::class,
                'model:vi' => MakeValidate::class,
                'model:faker' => MakeFaker::class,
                'model:comment' => MakeComment::class,
            ]
        );
    }
}