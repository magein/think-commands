<?php
declare (strict_types=1);

namespace magein\think\command;

use magein\utils\File;
use magein\utils\Variable;
use magein\think\command\traits\CommandParamParse;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument as InputArgument;
use think\console\Output;
use think\Model;

class MakeModelAttr extends Command
{

    use CommandParamParse;

    protected $help = "
    php think model:attr             识别Models/*.php文件
    php think model:attr user        识别Models/User.php文件
    php think model:attr user/order  识别Models/User/Order.php文件
    php think model:attr user/order_good  识别Models/User/OrderGood.php文件
";

    protected function configure()
    {
        // 指令配置
        $this->setName('model:attr')
            ->setDescription('生成模型的属性')
            ->addArgument('filename', InputArgument::OPTIONAL, '文件名称')
            ->setHelp($this->help);
    }

    protected function execute(Input $input, Output $output)
    {
        if (env('app_env') !== 'local') {
            $output->error('只能在开发环境中使用');
            exit();
        }

        // 指令输出
        $name = $input->getArgument('filename');

        $filepath = $this->app->config->get('console.config.model.path', '') ?: 'app/model';
        $filepath = trim($filepath, '/') . '/';
        $files = [];
        if ($name) {

            $names = explode('/', $name);
            $filename = array_pop($names);

            foreach ($names as $item) {
                $filepath .= Variable::ins()->camelCase($item) . '/';
            }

            $filepath .= Variable::ins()->pascal(trim($filename, '/')) . '.php';
            if (is_file($filepath)) {
                $files[] = $filepath;
            }
        } else {
            $result = File::ins()->getTreeList(trim($filepath, '/'), ['php']);
            $files = $result->getData();
        }

        if (empty($files)) {
            $output->error($filepath . '目录下没有模型文件');
            $output->error($this->help);
            exit();
        }

        foreach ($files as $path) {
            $content = file_get_contents($path);
            $namespace = preg_replace(['/\.\/app/', '/\.php/', '/\//'], ['app', '', '\\\\'], $path);

            if (preg_match('/\* @property/', $content)) {
                $output->comment('continue file : ' . $path);
                continue;
            }

            $model = new $namespace();
            $property = $this->make($model);

            // 替换属性
            $filename = pathinfo($path, PATHINFO_FILENAME);
            $content = preg_replace("/class $filename/", $property . "\n" . "class $filename", $content);
            file_put_contents($path, $content);
            $output->writeln('success file: ' . $path);
        }
    }

    /**
     * 生成模型属性
     * 支持外部调用
     * @param Model $model 模型实例
     * @return string
     */
    public function make(Model $model): string
    {
        $attrs = $this->tableAttrs($model);
        if (empty($attrs)) {
            return '';
        }

        $property = '/**';
        $property .= "\n";

        $cla_name = class_basename($model);
        $methods = "";
        $method_params = function ($prefix, $name, $param) {
            if ($prefix == '__') {
                $name = '\think\paginator\driver\Bootstrap';
            } elseif ($prefix == '___') {
                $name = '\think\model\Collection';
            } else {
                $name = Variable::ins()->pascal($name);
            }
            return ' * @method static ' . $name . '|null ' . $prefix . Variable::ins()->camelCase($param) . '($' . $param . ');' . "\n";
        };


        foreach ($attrs as $attr) {
            $field = $attr['Field'];
            $type = $attr['Type'];
            $key = $attr['Key'];
            if (in_array($field, ['id', 'created_at', 'updated_at', 'deleted_at'])) {
                continue;
            }
            if (preg_match('/int/', $type)) {
                $type = 'integer';
            } elseif (preg_match('/decimal/', $type)) {
                $type = 'float';
            } else {
                $type = 'string';
            }
            $property .= " * @property $type $" . $field;
            $property .= "\n";

            if (preg_match('/_id|_no|phone|email$/', $field) || $key) {
                $methods .= $method_params('_', $cla_name, $field);
                $methods .= $method_params('__', $cla_name, $field);
                $methods .= $method_params('___', $cla_name, $field);
            } elseif (preg_match('/^char/', $attr['Type'])) {
                $methods .= $method_params('_', $cla_name, $field);
                $methods .= $method_params('__', $cla_name, $field);
                $methods .= $method_params('___', $cla_name, $field);
            } elseif (preg_match('/tinyint/', $attr['Type'])) {
                $methods .= $method_params('__', $cla_name, $field);
                $methods .= $method_params('___', $cla_name, $field);
            }
        }
        $property .= $methods;
        $property = trim($property, "\n");
        $property .= "\n */";

        return $property;
    }
}