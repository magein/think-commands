<?php
declare (strict_types=1);

namespace magein\think\command;

use magein\think\command\traits\CommandParamParse;
use magein\utils\Faker;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use think\helper\Str;
use think\Model;

class MakeFaker extends Command
{
    use CommandParamParse;

    protected $help = '
    php think model:faker user                      根据model/User.php创建
    php think model:faker user_order                根据model/UserOrder.php创建
    php think model:faker user/user_order           根据model/user/UserOrder.php创建
';

    protected function configure()
    {
        // 指令配置
        $this->setName('model:faker')
            ->addArgument('name')
            ->addOption('--limit', 'l', Option::VALUE_OPTIONAL, '生成数量')
            ->setDescription('创建模拟数据')
            ->setHelp($this->help);

    }

    protected function execute(Input $input, Output $output)
    {
        if (env('app_env') !== 'local') {
            $output->error('只能在开发环境中使用');
            exit();
        }

        $name = $input->getArgument('name');
        $limit = intval($input->getOption('limit'));

        if ($limit <= 0) {
            $limit = 1;
        } elseif ($limit >= 100) {
            $limit = 100;
        }

        $success = $this->make($name, $limit);

        // 指令输出
        $output->writeln('创建完成，创建成功数量:' . $success);
    }

    /**
     * 创建数据
     * 允许外部调用
     * @param Model|string $name
     * @param string|int $limit
     * @return int
     */
    public function make($name, int $limit = 1): int
    {
        $limit = intval($limit);
        if ($limit <= 0) {
            $limit = 1;
        } elseif ($limit >= 100) {
            $limit = 100;
        }

        $config = app()->config->get('console.config.faker', []);
        $img_field = ($config['img_field'] ?? '') ?: ['avatar', 'head', 'icon', 'thumb', 'pic', 'pics', 'img', 'image', 'images', 'picture', 'photo'];
        $img_src = ($config['img_src'] ?? []) ?: [];

        $http_field = ($config['http_field'] ?? '') ?: ['url', 'urls', 'redirect', 'http', 'https', 'remote', 'remote_url', 'domain', 'host'];
        $http_url = ($config['http_url'] ?? '') ?: [env('app_url', 'http://localhost')];

        $attrs = $this->tableAttrs($name);

        if (is_string($name)) {
            $model_namespace = $this->modelNamespace($name);
        } elseif ($name instanceof Model) {
            $model_namespace = get_class($name);
        }
        $success = 0;
        for ($i = 0; $i < $limit; $i++) {
            /**
             * @var  Model $model
             */
            $model = new $model_namespace();

            $data = Faker::mock($attrs, function ($attr) use ($img_field, $http_field, $img_src, $http_url) {
                $field = $attr['Field'];
                $type = $attr['Type'];
                $comment = $attr['Comment'];
                if (in_array($field, $img_field)) {
                    return Faker::random($img_src);
                }
                if (in_array($field, $http_field)) {
                    return Faker::random($http_url);
                }
                return '';
            });
            $model->save($data);

            if ($model->id) {
                $success++;
            }
        }

        return $success;
    }
}