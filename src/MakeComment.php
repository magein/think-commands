<?php
declare (strict_types=1);

namespace magein\think\command;

use magein\think\command\traits\CommandParamParse;
use magein\utils\Apipost;
use magein\utils\Variable;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use think\Model;

class MakeComment extends Command
{

    use CommandParamParse;

    protected $help = '
    php think model:comment user                      根据model/User.php创建
    php think model:comment user_order                根据model/UserOrder.php创建
    php think model:comment user/user_order           根据model/user/UserOrder.php创建
    php think model:comment user/user_order -p        根据model/user/UserOrder.php创建
';

    protected function configure()
    {
        // 指令配置
        $this->setName('model:comment')
            ->addArgument('name')
            ->addOption('page', 'p', Option::VALUE_NONE, '返回的data是分页数据')
            ->addOption('list', 'l', Option::VALUE_NONE, '返回的data是列表数据')
            ->setDescription('根据数据库创建apiPost工具请求接口返回的字段说明')
            ->setHelp($this->help);
    }

    protected function execute(Input $input, Output $output)
    {

        if (env('app_env') !== 'local') {
            $output->error('只能在开发环境中使用');
            exit();
        }

        $name = $input->getArgument('name');
        $page = $input->getOption('page');
        $list = $input->getOption('list');

        $ext = '';
        if ($page) {
            $ext = '.page';
        } elseif ($list) {
            $ext = '.list';
        }
        $format = trim($ext ?: 'object', '.');

        $filepath = $this->filepath($name, 'comment', 'comments');
        $filename = $this->filename($name, 'comment');

        $filepath .= Variable::ins()->pascal(trim($filename, '/') . $ext . '.json');
        if (is_file($filepath)) {
            unlink($filepath);
        }
        $model = $this->model($name);
        if (empty($model) || !$model instanceof Model) {
            $output->error('实例化模型文件错误');
            exit();
        }

        $attrs = $this->tableAttrs($name);

        $apipost = new Apipost($attrs);
        if ($format == 'page') {
            $template = $apipost->page();
        } elseif ($format == 'list') {
            $template = $apipost->lists();
        } else {
            $template = $apipost->object();
        }

        file_put_contents($filepath, $template);

        // 指令输出
        $output->writeln('创建完成:' . $filepath);
    }
}